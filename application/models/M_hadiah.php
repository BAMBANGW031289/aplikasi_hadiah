<?php

class M_hadiah extends CI_Model{
	protected $_table = 'hadiah';

	public function lihat(){
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function jumlah(){
		$query = $this->db->get($this->_table);
		return $query->num_rows();
	}

	public function lihat_id($kode_hadiah){
		$query = $this->db->get_where($this->_table, ['kode_hadiah' => $kode_hadiah]);
		return $query->row();
	}

	public function lihat_nama_hadiah($nama_hadiah){
		$query = $this->db->select('*');
		$query = $this->db->where(['nama_hadiah' => $nama_hadiah]);
		$query = $this->db->get($this->_table);
		return $query->row();
	}

	public function tambah($data){
		return $this->db->insert($this->_table, $data);
	}

	public function min_stok($stok, $nama_hadiah){
		$query = $this->db->set('stok', 'stok-' . $stok, false);
		$query = $this->db->where('nama_hadiah', $nama_hadiah);
		$query = $this->db->update($this->_table);
		return $query;
	}

	public function ubah($data, $kode_hadiah){
		$query = $this->db->set($data);
		$query = $this->db->where(['kode_hadiah' => $kode_hadiah]);
		$query = $this->db->update($this->_table);
		return $query;
	}

	public function hapus($kode_hadiah){
		return $this->db->delete($this->_table, ['kode_hadiah' => $kode_hadiah]);
	}
}
