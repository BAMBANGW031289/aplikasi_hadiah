<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('partials/head.php') ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<!-- load sidebar -->
		<?php $this->load->view('partials/sidebar.php') ?>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content" data-url="<?= base_url('hadiah') ?>">
				<!-- load Topbar -->
				<?php $this->load->view('partials/topbar.php') ?>

				<div class="container-fluid">
				<div class="clearfix">
					<div class="float-left">
						<h1 class="h3 m-0 text-gray-800"><?= $title ?></h1>
					</div>
					<div class="float-right">
						<a href="<?= base_url('hadiah') ?>" class="btn btn-secondary btn-sm"><i class="fa fa-reply"></i>&nbsp;&nbsp;Kembali</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="card shadow">
							<div class="card-header"><strong>Isi Form Dibawah Ini!</strong></div>
							<div class="card-body">
								<form action="<?= base_url('hadiah/proses_tambah') ?>" id="form-tambah" method="POST" enctype="multipart/form-data">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="kode_hadiah"><strong>Kode Hadiah</strong></label>
											<input type="text" name="kode_hadiah" placeholder="Masukkan Kode Hadiah" autocomplete="off"  class="form-control" required value="HDH<?= mt_rand(1000000, 9999999) ?>" maxlength="7" readonly>
										</div>
										<div class="form-group col-md-6">
											<label for="nama_hadiah"><strong>Nama Hadiah</strong></label>
											<input type="text" name="nama_hadiah" placeholder="Masukkan Nama Hadiah" autocomplete="off"  class="form-control" required>
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-12">
											<label for="alamat"><strong>Foto</strong></label>
											<input type="file" name="foto" placeholder="Masukkan foto Hadiah" autocomplete="off"  class="form-control" required>
										</div>

									</div>
										<div class="form-row">
									<div class="form-group col-md-6">
										<label for="nama_hadiah"><strong>Point</strong></label>
										<input type="text" name="point" placeholder="Masukkan Point" value="" autocomplete="off"  class="form-control" required>
									</div>
								</div>
								</div>

									<hr>
									<div class="form-group">
										<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
										<button type="reset" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- load footer -->
			<?php $this->load->view('partials/footer.php') ?>
		</div>
	</div>
	<?php $this->load->view('partials/js.php') ?>
</body>
</html>
