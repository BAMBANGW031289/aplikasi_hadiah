/*
 Navicat Premium Data Transfer

 Source Server         : Mysql@Bams
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : db_hadiah

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 19/07/2020 15:42:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_customer` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_customer` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_telepon` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username_customer` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password_customer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (4, 'CUS8430153', 'Herawati', 'herawati_104@gmail.com', '021-87772313', 'Jl. Pelangi No 124', NULL, NULL);
INSERT INTO `customer` VALUES (8, 'CUS1175488', 'Khairunnisa', 'khairun@gmail.com', '0821444499999', 'Jl. Sawo matang no 9', NULL, NULL);
INSERT INTO `customer` VALUES (9, 'CUS1580785', 'Aidil Iskandar', 'aidil@gmail.com', '09213232', 'Jl. pattimura no 89', NULL, NULL);
INSERT INTO `customer` VALUES (10, 'CUS4144146', 'Alfin Andri', 'alfin@gmail.com', '0873231111999', 'Jl Kangkung no 14', NULL, NULL);
INSERT INTO `customer` VALUES (11, 'CUS6187946', 'Baruna', 'baruna@gmail.com', '093234124', 'jl. bali no 90', 'baruna123', '47bce5c74f589f4867dbd57e9ca9f808');
INSERT INTO `customer` VALUES (12, 'CUS7305439', 'Okky salim', 'okky@gmail.com', '08888211313', 'Jl. Bungur no 2', 'okky123', '47bce5c74f589f4867dbd57e9ca9f808');
INSERT INTO `customer` VALUES (13, 'CUS1638836', 'Khofifah', 'kofifah@gmail.com', '0932321222', 'Jl. Mejangan no 67', 'kofi123', '47bce5c74f589f4867dbd57e9ca9f808');
INSERT INTO `customer` VALUES (14, 'CUS1773622', 'Customer123', 'customer@gmail.com', '087873213231', 'Jl. customer123', 'custom123', '47bce5c74f589f4867dbd57e9ca9f808');

-- ----------------------------
-- Table structure for hadiah
-- ----------------------------
DROP TABLE IF EXISTS `hadiah`;
CREATE TABLE `hadiah`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_hadiah` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_hadiah` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `point` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hadiah
-- ----------------------------
INSERT INTO `hadiah` VALUES (17, 'HDH2431714', 'kulkas maspion', './assets/upload/b04a43ca3fa30606562b1f8bc140b93a.jpg', 22);
INSERT INTO `hadiah` VALUES (18, 'HDH6535411', 'Magic com yongma', './assets/upload/7e585a99dc5a0b4fcf0980edbc1b5eab.jpg', 34);
INSERT INTO `hadiah` VALUES (19, 'HDH3191322', 'kompor gas maspion', './assets/upload/7678e8bed0ec818ec06e6026cf8b0887.jpg', 18);

-- ----------------------------
-- Table structure for keys
-- ----------------------------
DROP TABLE IF EXISTS `keys`;
CREATE TABLE `keys`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of keys
-- ----------------------------
INSERT INTO `keys` VALUES (1, 1, 'wisnu123', 1, 0, 0, NULL, 0);

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(23) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_admin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_transaksi` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jam_transaksi` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `point` int(11) NULL DEFAULT NULL,
  `nama_customer` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES (34, 'TR1595039024', 'Administrator', '18/07/2020', '09:23:44', 5, 'CUS6187946');
INSERT INTO `transaksi` VALUES (35, 'TR1595039033', 'Administrator', '18/07/2020', '09:23:53', 5, 'CUS6187946');
INSERT INTO `transaksi` VALUES (36, 'TR1595039041', 'Administrator', '18/07/2020', '09:24:01', 5, 'CUS7305439');
INSERT INTO `transaksi` VALUES (37, 'TR1595039049', 'Administrator', '18/07/2020', '09:24:09', 5, 'CUS7305439');
INSERT INTO `transaksi` VALUES (38, 'TR1595039057', 'Administrator', '18/07/2020', '09:24:17', 5, 'CUS7305439');
INSERT INTO `transaksi` VALUES (39, 'TR1595056896', 'Administrator', '18/07/2020', '14:21:36', 5, 'CUS7305439');
INSERT INTO `transaksi` VALUES (40, 'TR1595146417', 'Administrator', '19/07/2020', '15:13:37', 5, 'CUS1773622');
INSERT INTO `transaksi` VALUES (41, 'TR1595146480', 'Administrator', '19/07/2020', '15:14:40', 5, 'CUS1773622');
INSERT INTO `transaksi` VALUES (42, 'TR1595146487', 'Administrator', '19/07/2020', '15:14:47', 5, 'CUS1773622');
INSERT INTO `transaksi` VALUES (43, 'TR1595146494', 'Administrator', '19/07/2020', '15:14:54', 5, 'CUS1773622');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_user` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_user` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (7, 'ADMN-13', 'Administrator', 'admin123', '47bce5c74f589f4867dbd57e9ca9f808');
INSERT INTO `user` VALUES (8, 'ADMN-14', 'kolili', 'kolili123', 'daca5a0f75e4bffafd0856e3d28a0856');
INSERT INTO `user` VALUES (9, 'ADMN-25', 'Alfin', 'alfin123', '8606c00dd0519823dd31e565780eb496');

-- ----------------------------
-- View structure for v_cust_user
-- ----------------------------
DROP VIEW IF EXISTS `v_cust_user`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_cust_user` AS SELECT
	customer.*, 
	`user`.kode_user, 
	`user`.nama_user, 
	`user`.username_user, 
	`user`.password_user
FROM
	customer
	LEFT JOIN
	`user`
	ON 
		customer.kode_customer = `user`.kode_user ;

SET FOREIGN_KEY_CHECKS = 1;
