<?php

class M_customer extends CI_Model{
	protected $_table = 'customer';
	protected $_table2 = 'v_cust_user';

	public function lihat(){
		$query = $this->db->get($this->_table);
		return $query->result();
	}

	public function jumlah(){
		$query = $this->db->get($this->_table);
		return $query->num_rows();
	}

	public function lihat_id($kode_customer){
		$query = $this->db->get_where($this->_table, ['kode_customer' => $kode_customer]);
		return $query->row();
	}

	public function lihat_nama_customer($nama_customer){
		$query = $this->db->select('*');
		$query = $this->db->where(['nama_customer' => $nama_customer]);
		$query = $this->db->get($this->_table);
		return $query->row();
	}

	public function lihat_customer($username){
		$query = $this->db->select('*');
		$query = $this->db->where(['username_customer' => $username]);
		$query = $this->db->get($this->_table);
		return $query->row();
	}

	public function tambah($data){
		return $this->db->insert($this->_table, $data);
	}

	public function min_stok($stok, $nama_customer){
		$query = $this->db->set('stok', 'stok-' . $stok, false);
		$query = $this->db->where('nama_customer', $nama_customer);
		$query = $this->db->update($this->_table);
		return $query;
	}

	public function ubah($data, $kode_customer){
		$query = $this->db->set($data);
		$query = $this->db->where(['kode_customer' => $kode_customer]);
		$query = $this->db->update($this->_table);
		return $query;
	}

	public function hapus($kode_customer){
		return $this->db->delete($this->_table, ['kode_customer' => $kode_customer]);
	}
}
