<?php

class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->login) redirect('dashboard');
		$this->load->model('M_kasir', 'm_kasir');
		$this->load->model('M_user', 'm_user');
		$this->load->model('m_customer', 'm_customer');
	}

	public function index(){
		$this->load->view('login');
	}

	public function proses_login(){
		if($this->input->post('role') === 'customer') $this->_proses_login_customer($this->input->post('username'));
		elseif($this->input->post('role') === 'admin') $this->_proses_login_admin($this->input->post('username'));
	//	elseif($this->input->post('role') === 'customer') $this->_proses_login_customer($this->input->post('username'));
		else {
			?>
			<script>
				alert('role tidak tersedia!')
			</script>
			<?php
		}
	}

	protected function _proses_login_kasir($username){
		$get_kasir = $this->m_kasir->lihat_username($username);
		if($get_kasir){
			if($get_kasir->password_kasir == $this->input->post('password')){
				$session = [
					'kode' => $get_kasir->kode_kasir,
					'nama' => $get_kasir->nama_kasir,
					'username' => $get_kasir->username_kasir,
					'password' => $get_kasir->password_kasir,
					'role' => $this->input->post('role'),
					'jam_masuk' => date('H:i:s')
				];

				$this->session->set_userdata('login', $session);
				$this->session->set_flashdata('success', '<strong>Login</strong> Berhasil!');
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('error', 'Password Salah!');
				redirect();
			}
		} else {
			$this->session->set_flashdata('error', 'Username Salah!');
			redirect();
		}
	}

	protected function _proses_login_customer($username){
		$get_user = $this->m_customer->lihat_customer($username);
		if($get_user){
			if($get_user->password_customer == md5($this->input->post('password'))){
				$session = [
					'kode' => $get_user->kode_customer,
					'nama' => $get_user->nama_customer,
					'username' => $get_user->username_customer,
					'password' => $get_user->password_customer,
					'role' => $this->input->post('role'),
					'jam_masuk' => date('H:i:s')
				];

				$this->session->set_userdata('login', $session);
				$this->session->set_flashdata('success', '<strong>Login</strong> Berhasil!');
				redirect('dashboard/liadaja');
			} else {
				$this->session->set_flashdata('error', 'Password Salah!');
				redirect();
			}
		} else {
			$this->session->set_flashdata('error', 'Username Salah!');
			redirect();
		}
	}

	protected function _proses_login_admin($username){
		$get_user = $this->m_user->lihat_username($username);
		if($get_user){
			if($get_user->password_user == md5($this->input->post('password'))){
				$session = [
					'kode' => $get_user->kode_user,
					'nama' => $get_user->nama_user,
					'username' => $get_user->username_user,
					'password' => $get_user->password_user,
					'role' => $this->input->post('role'),
					'jam_masuk' => date('H:i:s')
				];

				$this->session->set_userdata('login', $session);
				$this->session->set_flashdata('success', '<strong>Login</strong> Berhasil!');
				redirect('dashboard');
			} else {
				$this->session->set_flashdata('error', 'Password Salah!');
				redirect();
			}
		} else {
			$this->session->set_flashdata('error', 'Username Salah!');
			redirect();
		}
	}

	public function registrasi(){
		// if ($this->session->login['role'] == 'kasir'){
		// 	$this->session->set_flashdata('error', 'Tambah data hanya untuk admin!');
		// 	redirect('transaksi');
		// }

		$data = [
			'kode_customer' => $this->input->post('kode_customer'),
			'nama_customer' => $this->input->post('nama_customer'),
			'alamat' => $this->input->post('alamat'),
			'no_telepon' => $this->input->post('no_telepon'),
			'email' => $this->input->post('email'),
			'username_customer' => $this->input->post('username'),
			'password_customer' => md5($this->input->post('password')),
		];

		// $data2 = [
		// 	'kode_user' => $this->input->post('kode_customer'),
		// 	'nama_user' => $this->input->post('nama_customer'),
		// 	'username_user' => $this->input->post('username'),
		// 	'password_user' => md5($this->input->post('password')),
		// ];

		if($this->m_customer->tambah($data) && $this->m_customer->tambah($data)){
			$this->session->set_flashdata('success', 'Data Akun <strong>Berhasil</strong> Ditambahkan!');
			redirect('login');
		} else {
			$this->session->set_flashdata('error', 'Data Akun <strong>Gagal</strong> Ditambahkan!');
			redirect('login');
		}
	}





}
