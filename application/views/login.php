<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Login :: AppWebHadiah</title>
	<link href="<?= base_url('sb-admin') ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link href="<?= base_url('sb-admin') ?>/css/sb-admin-2.min.css" rel="stylesheet">
	<link href="<?= base_url('sb-admin') ?>/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">


</head>



<body class="bg-gradient-primary">

	<div class="container">

		<!-- Outer Row -->
		<div class="row justify-content-center">

			<div class="col-lg-6">

				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col-lg-12">
								<div class="p-5">
									<?php if ($this->session->flashdata('success')) : ?>
										<div class="alert alert-success alert-dismissible fade show" role="alert">
											<?= $this->session->flashdata('success') ?>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									<?php elseif($this->session->flashdata('error')) : ?>
										<div class="alert alert-danger alert-dismissible fade show" role="alert">
											<?= $this->session->flashdata('error') ?>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									<?php endif ?>
									<div id="login" style="display:block;">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">Silahkan Login!</h1>
									</div>
									<form class="user" method="POST" action="<?= base_url('login/proses_login') ?>">
										<div class="form-group logins">Username
											<input type="text" class="form-control" id="username" onkeyup="emails()" placeholder="Masukkan Username" autocomplete="off" required name="username">
										</div>
										<div id="valid"></div>
										<div class="form-group">Password
										<div class="input-group">
											<input type="password" class="form-control" id="passwordL" placeholder="Masukkan Password" required name="password">
											<div class="input-group-append">
												<input type=button id="show2" value="&#xf070;" onclick="ShowPassword2()" class="btn btn-danger btn-xs pull-right fas fa-500px">
												<input type=button style="display:none" id="hide2" value="&#xf06e;" onclick="HidePassword2()" class="btn btn-danger btn-xs pull-right fas fa-500px">

									 </div>
										</div>
										</div>
										<div class="form-group">Sebagai
											<select name="role" id="rolev" class="form-control" required>
												<option value="">Masuk Sebagai</option>
												<option value="customer">Customer</option>
												<option value="admin">Admin</option>
											</select>
										</div>


										<button type="submit" class="btn btn-primary btn-block" name="login">
											Login
										</button>
									</form>
									<div class="text-center">
                <a class="small" href="#" onclick="register()" >Perlu Mendaftar Akun Baru!</a>
              </div>
								</div>
								<div id="register" style="display:none;">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">Register!</h1>
									</div>
								<form class="user" method="POST" action="<?= base_url('login/registrasi') ?>">
									<input type="hidden" name="kode_user" placeholder="Masukkan Kode User" autocomplete="off"  class="form-control" required value="USER-<?= mt_rand(10, 99) ?>" maxlength="8" readonly>

											<input type="hidden" name="kode_customer" placeholder="Masukkan Kode Customer" autocomplete="off"  class="form-control" required value="CUS<?= mt_rand(1000000, 9999999) ?>" maxlength="7" readonly>
									<div class="form-group">Nama Lengkap
										<input type="text" class="form-control" id="namalengk" placeholder="Masukkan Nama Lengkap" autocomplete="off" required name="nama_customer">
									</div>
									<div class="form-group">Alamat
										<input type="text" class="form-control" id="alamat" placeholder="Masukkan Alamat" autocomplete="off" required name="alamat">
									</div>
									<div class="form-group">No Telp
										<input type="text" class="form-control" id="no_telp" placeholder="Masukkan No telepon" autocomplete="off" required name="no_telepon">
									</div>
									<div class="form-group">Email
										<input type="email" class="form-control" id="email"  placeholder="Masukkan Email" autocomplete="off" required name="email">
									</div>
									<div class="form-group">Username (*maksimal 10 karakter)
										<input type="text" class="form-control" id="usernamec" placeholder="Masukkan username" autocomplete="off" maxlength="10" required name="username">
									</div>
									<div class="form-group">Password
									<div class="input-group">
										<input type="password" class="form-control" id="passwordx" placeholder="Masukkan Password" required name="password">
										<div class="input-group-append">
											<input type=button id="show" value="&#xf070;" onclick="ShowPassword()" class="btn btn-danger btn-xs pull-right fas fa-500px">
	 										<input type=button style="display:none" id="hide" value="&#xf06e;" onclick="HidePassword()" class="btn btn-danger btn-xs pull-right fas fa-500px">

								 </div>
									</div>
									</div>
									<div class="form-group">
										<select name="role" id="role" class="form-control" required hidden>
											<!-- <option value="">Masuk Sebagai</option> -->
											<option value="customer">Customer</option>
											<!-- <option value="admin">Admin</option> -->
										</select>
									</div>
									<button type="submit" class="btn btn-primary btn-block" name="login">
										Register
									</button>
								</form>
								  <a class="small" href="#" onclick="login()" >Silahkan coba Login!</a>
							</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	<script src="<?= base_url('sb-admin') ?>/vendor/jquery/jquery.min.js"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="<?= base_url('sb-admin') ?>/js/sb-admin-2.min.js"></script>
		<script src="<?= base_url('sb-admin') ?>/vendor/bootstrap/js/bootstrap-show-password.js"></script>
			<script src="<?= base_url('sb-admin') ?>/vendor/bootstrap/js/bootstrap-show-password.min.js"></script>

	<script>
	function register() {
      var x = document.getElementById("register");
			var y = document.getElementById("login");
      if 	(x.style.display === "none") {
        x.style.display = "block";
				y.style.display = "none";
      } 	else {
        y.style.display = "none";
      }
  }

	function login() {
			var y = document.getElementById("register");
			var x = document.getElementById("login");
			if 	(x.style.display === "none") {
				x.style.display = "block";
				y.style.display = "none";
			} 	else {
				y.style.display = "none";
			}
	}

	function ShowPassword()
					{
						if(document.getElementById("passwordx").value!="")
						{
						document.getElementById("passwordx").type="text";
						document.getElementById("show").style.display="none";
						document.getElementById("hide").style.display="block";
						}
					}

function ShowPassword2()
					{

						if(document.getElementById("passwordL").value!="")
						{
						document.getElementById("passwordL").type="text";
						document.getElementById("show2").style.display="none";
						document.getElementById("hide2").style.display="block";
						}
					}

					function HidePassword()
					{
						if(document.getElementById("passwordx").type == "text")
						{
						document.getElementById("passwordx").type="password"
						document.getElementById("show").style.display="block";
						document.getElementById("hide").style.display="none";
						}

					}


          function HidePassword2()
					{
						if(document.getElementById("passwordL").type == "text")
						{
						document.getElementById("passwordL").type="password"
						document.getElementById("show2").style.display="block";
						document.getElementById("hide2").style.display="none";
						}
					}

					// $('.logins input:first').on('keyup', function(){
					//     var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
					//    $('#valid').html('It\'s'+ (valid?'':' not') +' valid');
					// 	 //$('#role').value=(valid?'customer');
					// 	 var opt = $("option[val=customer]"),
					// 		    html = $("#role").append(opt.clone()).html();
					// 		html = html.replace(/\>/, valid?' selected="selected">');
					// 		opt.replaceWith(html);
					// });


	</script>
