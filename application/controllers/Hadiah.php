<?php

use Dompdf\Dompdf;

class Hadiah extends CI_Controller{
	public function __construct(){
		parent::__construct();
		if($this->session->login['role'] != 'kasir' && $this->session->login['role'] != 'admin') redirect();
		$this->data['aktif'] = 'hadiah';
		$this->load->model('M_hadiah', 'm_hadiah');
	}

	public function index(){
		$this->data['title'] = 'Data Hadiah';
		$this->data['all_hadiah'] = $this->m_hadiah->lihat();
		$this->data['no'] = 1;

		$this->load->view('hadiah/lihat', $this->data);
	}

	public function liadaja(){
		$this->data['title'] = 'Data Hadiah';
		$this->data['all_hadiah'] = $this->m_hadiah->lihat();
		$this->data['no'] = 1;

		$this->load->view('hadiah/detail_hadiah', $this->data);
	}

	public function tambah(){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Tambah data hanya untuk admin!');
			redirect('transaksi');
		}

		$this->data['title'] = 'Tambah Hadiah';

		$this->load->view('hadiah/tambah', $this->data);
	}

	public function proses_tambah(){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Tambah data hanya untuk admin!');
			redirect('transaksi');
		}

		// $config = array(
		// 			 'upload_path' => './assets/upload',
		// 			 'allowed_types' => 'gif|jpg|JPG|png|jpeg|pdf|xls',
		// 			 'max_size' => '3072',
		//
		// 	 );

			 	$config['upload_path']   = './assets/upload';
      	$config['allowed_types'] = 'gif|jpg|JPG|png|jpeg|pdf|xls';
   	 		$config['max_size']      = 1000000;
 	 			$config['max_width']     = 1024;
 	 			$config['max_height']    = 768;
    		$config['encrypt_name']     = TRUE;


			 $this->load->library('upload', $config);
			 $this->upload->do_upload('foto');
			 $upload_foto = $this->upload->data();
	 	   $file_name = $upload_foto['file_name'];

		$data = [
			'kode_hadiah' => $this->input->post('kode_hadiah'),
			'nama_hadiah' => $this->input->post('nama_hadiah'),
			'foto' => $config['upload_path']."/".$file_name,
			'point' => $this->input->post('point'),

		];

	//	print_r($file_name);

		if($this->m_hadiah->tambah($data)){
			$this->session->set_flashdata('success', 'Data Hadiah <strong>Berhasil</strong> Ditambahkan!');
			redirect('hadiah');
		} else {
			$this->session->set_flashdata('error', 'Data Hadiah <strong>Gagal</strong> Ditambahkan!');
			redirect('hadiah');
		}
	}

	public function ubah($kode_hadiah){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Ubah data hanya untuk admin!');
			redirect('transaksi');
		}

		$this->data['title'] = 'Ubah Hadiah';
		$this->data['hadiah'] = $this->m_hadiah->lihat_id($kode_hadiah);

		$this->load->view('hadiah/ubah', $this->data);
	}

	public function proses_ubah($kode_hadiah){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Ubah data hanya untuk admin!');
			redirect('transaksi');
		}

		$config['upload_path']   = './assets/upload';
		$config['allowed_types'] = 'gif|jpg|JPG|png|jpeg|pdf|xls';
		$config['max_size']      = 1000000;
		$config['max_width']     = 1024;
		$config['max_height']    = 768;
		$config['encrypt_name']     = TRUE;


	 $this->load->library('upload', $config);
	 $this->upload->do_upload('foto');
	 $upload_foto = $this->upload->data();
	 $file_name = $upload_foto['file_name'];

$data = [
	'kode_hadiah' => $this->input->post('kode_hadiah'),
	'nama_hadiah' => $this->input->post('nama_hadiah'),
	'foto' => $config['upload_path']."/".$file_name,
	'point' => $this->input->post('point'),

];

		if($this->m_hadiah->ubah($data, $kode_hadiah)){
			$this->session->set_flashdata('success', 'Data Hadiah <strong> '.$data['kode_hadiah'].' Berhasil</strong> Diubah!');
			redirect('hadiah');
		} else {
			$this->session->set_flashdata('error', 'Data Hadiah <strong>Gagal</strong> Diubah!');
			redirect('hadiah');
		}
	}

	public function hapus($kode_hadiah){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Hapus data hanya untuk admin!');
			redirect('transaksi');
		}

		if($this->m_hadiah->hapus($kode_hadiah)){
			$this->session->set_flashdata('success', 'Data Hadiah <strong>Berhasil</strong> Dihapus!');
			redirect('hadiah');
		} else {
			$this->session->set_flashdata('error', 'Data Hadiah <strong>Gagal</strong> Dihapus!');
			redirect('hadiah');
		}
	}

	public function export(){
		$dompdf = new Dompdf();
		// $this->data['perusahaan'] = $this->m_usaha->lihat();
		$this->data['all_hadiah'] = $this->m_hadiah->lihat();
		$this->data['title'] = 'Laporan Data Hadiah';
		$this->data['no'] = 1;

		$dompdf->setPaper('A4', 'Landscape');
		$html = $this->load->view('hadiah/report', $this->data, true);
		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream('Laporan Data Hadiah Tanggal ' . date('d F Y'), array("Attachment" => false));
	}
}
