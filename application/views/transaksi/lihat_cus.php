<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('partials/head.php') ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<!-- load sidebar -->
		<?php $this->load->view('partials/sidebar.php') ?>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content" data-url="<?= base_url('transaksi') ?>">
				<!-- load Topbar -->
				<?php $this->load->view('partials/topbar.php') ?>

				<div class="container-fluid">
				<div class="clearfix">
					<div class="float-left">
						<h1 class="h3 m-0 text-gray-800">Transaksi</h1>
					</div>
					<div class="float-right">
						<!-- <a href="<?= base_url('transaksi/export') ?>" class="btn btn-danger btn-sm"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;Export</a> -->
						<!-- <a href="<?= base_url('transaksi/tambah') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah</a> -->
					</div>
				</div>
				<hr>
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<?= $this->session->flashdata('success') ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php elseif($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<?= $this->session->flashdata('error') ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php endif ?>
				<div class="card shadow">
					<div class="card-header"><strong>Daftar Transaksi</strong></div>
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<td>No Transaksi</td>
										<td>Nama Customer</td>
										<td>Nama Admin</td>
										<td>Tanggal Transaksi</td>
										<td>Point</td>
									</tr>
								</thead>
								<tbody>
									<?php $qty=0; foreach ($all_transaksi2 as $transaksi):
										$qty += $transaksi['point']; ?>
										<tr>
											<td><?= $transaksi['no_transaksi']; ?></td>
											<td><?= $transaksi['nama_customer']; ?></td>
											<td><?= $transaksi['nama_admin'] ?></td>
											<td><?= $transaksi['tgl_transaksi']; ?></'] ?> || Pukul <?= $transaksi['jam_transaksi']; ?> WIB </td>
											<td><?= $transaksi['point']; ?></td>
										</tr>
									<?php endforeach ?>
								</tbody>
								<tr bgcolor="darkgrey"><td colspan="4" style="text-align:right; color:white;"  width="140"><b>TOTAL POINT</b></td><td style="color:white;" width="10"><b> <?php echo $qty  ?></b></td></tr>
								</tr>
							</table>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- load footer -->
			<?php $this->load->view('partials/footer.php') ?>
		</div>
	</div>
	<?php $this->load->view('partials/js.php') ?>
	<script src="<?= base_url('sb-admin/js/demo/datatables-demo.js') ?>"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
</body>
</html>
