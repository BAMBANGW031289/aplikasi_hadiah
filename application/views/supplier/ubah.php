<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('partials/head.php') ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<!-- load sidebar -->
		<?php $this->load->view('partials/sidebar.php') ?>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content" data-url="<?= base_url('supplier') ?>">
				<!-- load Topbar -->
				<?php $this->load->view('partials/topbar.php') ?>

				<div class="container-fluid">
				<div class="clearfix">
					<div class="float-left">
						<h1 class="h3 m-0 text-gray-800"><?= $title ?></h1>
					</div>
					<div class="float-right">
						<a href="<?= base_url('supplier') ?>" class="btn btn-secondary btn-sm"><i class="fa fa-reply"></i>&nbsp;&nbsp;Kembali</a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="card shadow">
							<div class="card-header"><strong>Isi Form Dibawah Ini!</strong></div>
							<div class="card-body">
								<form action="<?= base_url('supplier/proses_ubah/' . $supplier->kode_supplier) ?>" id="form-tambah" method="POST">
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="kode_supplier"><strong>Kode Supplier</strong></label>
											<input type="text" name="kode_supplier" placeholder="Masukkan Kode Supplier" autocomplete="off"  class="form-control" required value="<?= $supplier->kode_supplier ?>" maxlength="8" readonly>
										</div>
										<div class="form-group col-md-6">
											<label for="nama_supplier"><strong>Nama Supplier</strong></label>
											<input type="text" name="nama_supplier" placeholder="Masukkan Nama Supplier" autocomplete="off"  class="form-control" required value="<?= $supplier->nama_supplier ?>">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="harga_beli"><strong>Alamat</strong></label>
											<input type="text" name="alamat" placeholder="Masukkan alamat Supplier" autocomplete="off"  class="form-control" required value="<?= $supplier->alamat ?>">
										</div>
										<div class="form-group col-md-6">
											<label for="harga_jual"><strong>No Telepon</strong></label>
											<input type="text" name="no_telepon" placeholder="Masukkan no_telepon Supplier" autocomplete="off"  class="form-control" required value="<?= $supplier->no_telepon ?>">
										</div>
									</div>
									<div class="form-row">
										<div class="form-group col-md-6">
											<label for="stok"><strong>Email</strong></label>
											<input type="text" name="stok" placeholder="Masukkan Stok" autocomplete="off"  class="form-control" required value="<?= $supplier->email ?>">
										</div>

									</div>
									<hr>
									<div class="form-group">
										<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
										<button type="reset" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- load footer -->
			<?php $this->load->view('partials/footer.php') ?>
		</div>
	</div>
	<?php $this->load->view('partials/js.php') ?>
</body>
</html>
