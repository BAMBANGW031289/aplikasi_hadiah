<?php

class PdfGenerator
{
  public function generate($html,$filename)
  {
    define('DOMPDF_ENABLE_AUTOLOAD', false);
    require_once("Dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
    //$dompdf->setPaper('A4', 'Landscape');
    $dompdf->load_html($html);
    $dompdf->render();
    //$dompdf->stream($filename.'.pdf',array("Attachment"=>0));
    $dompdf->stream('Laporan Detail Penjualan Tanggal ' . date('d F Y'), array("Attachment" => false));
  }
}
