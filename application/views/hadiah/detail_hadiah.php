<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('partials/head.php') ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<!-- load sidebar -->
		<?php $this->load->view('partials/sidebar.php') ?>

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content" data-url="<?= base_url('hadiah') ?>">
				<!-- load Topbar -->
				<?php $this->load->view('partials/topbar.php') ?>

				<div class="container-fluid">
				<div class="clearfix">
					<div class="float-left">
						<h1 class="h3 m-0 text-gray-800"><?= $title ?></h1>
					</div>



					<div class="float-right">
						<!-- <?//= echo $points; ?> -->
						<!-- <a href="<?= base_url('hadiah/export_detail/' . $hadiah->no_hadiah) ?>" class="btn btn-danger btn-sm"><i class="fa fa-file-pdf"></i>&nbsp;&nbsp;Export</a> -->
						<a href="<?= base_url('hadiah') ?>" class="btn btn-secondary btn-sm"><i class="fa fa-reply"></i>&nbsp;&nbsp;Kembali</a>
					</div>
				</div>

				<!-- Point anda yang terkumpul hingga saat ini : <?= $poin ?> -->

				<hr>
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<?= $this->session->flashdata('success') ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php elseif($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<?= $this->session->flashdata('error') ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				<?php endif ?>
				<div class="card shadow">
						<div class="card-header"><strong>No Customer : <?= $pointss ?> </strong></div>
				  <div class="card-body">
						<div class="row justify-content-md-center">
							<div class="col-md-4" align="center">
						<span style="font-size:20px">Poin Anda Saat ini : </span><p>

						<span style="font-size:200px" align="center" ><b><?= $poin?></b></span>



             	</div>
								</div>
					</div>
				</div>
				<br>



				<!-- <div class="card shadow">

					<div class="card-header"><strong>No Hadiah : <?= $hadiah->kode_hadiah ?> </strong></div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">

								<table class="table table-borderless">

									<tr>
										<td><strong>No Hadiah</strong></td>
										<td>:</td>
										<td><?= $hadiah->kode_hadiah ?></td>
									</tr>
									<tr>
										<td><strong>Nama Hadiah</strong></td>
										<td>:</td>
										<td><?= $hadiah->nama_hadiah?></td>
									</tr>
									<tr>
										<td><strong>Foto</strong></td>
										<td>:</td>
										<td><img width="60%" height="200" src="<?= base_url() ?><?= $hadiah->foto ?>" class="img-responsive"></td>
									</tr>
									<tr>
										<td><strong>Point</strong></td>
										<td>:</td>
										<td><?= $hadiah->point ?></td>
									</tr>

								</table>
							</div> -->
						<!-- </div> -->
						<hr>
						<!-- <div class="row">
							<div class="col-md-12">
								<table class="table table-bordered">
									<thead>
										<tr>
											<td><strong>No</strong></td>
											<td><strong>Nama Barang</strong></td>
											<td><strong>Harga Barang</strong></td>
											<td><strong>Jumlah</strong></td>
											<td><strong>Sub Total</strong></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($all_detail_hadiah as $detail_hadiah): ?>
											<tr>
												<td><?= $no++ ?></td>
												<td><?= $detail_hadiah->nama_barang ?></td>
												<td>Rp <?= number_format($detail_hadiah->harga_barang, 0, ',', '.') ?></td>
												<td><?= $detail_hadiah->jumlah_barang ?> <?= strtoupper($detail_hadiah->satuan) ?></td>
												<td>Rp <?= number_format($detail_hadiah->sub_total, 0, ',', '.') ?></td>
											</tr>
										<?php endforeach ?>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="4" align="right"><strong>Total : </strong></td>
											<td>Rp <?= number_format($hadiah->total, 0, ',', '.') ?></td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div> -->
					<!-- </div>
				</div> -->
				<!-- <?//php endforeach ?> -->
				<div class="card-deck">
				  <?php foreach ($all_hadiah as $hadiah): ?>
				  <div class="card">
				    <img width="100" height="500" class="card-img-top" src="<?= base_url() ?><?= $hadiah->foto ?>" alt="Card image cap">
				    <div class="card-body">
				      <h5 class="card-title"><strong>No Hadiah : <?= $hadiah->kode_hadiah ?> </strong></h5>
				      <p class="card-text"><?= $hadiah->nama_hadiah?></p>
				    </div>
				    <div class="card-footer text-right">
				   <small class="text-muted"><?= $hadiah->point ?> Point</small>&nbsp;&nbsp;
					 <?php $p = $poin;
					     $hp = $hadiah->point;
            if($p > $hp):
					 ?>
					 <a href="" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;&nbsp; Tercukupi</a>
				 <?php else: ?>
					 <a href="" class="btn btn-danger btn-sm"><i class="fa fa-ban"></i>&nbsp;&nbsp; Belum Cukup</a>
					 <?php endif; ?>
				 </div>
				  </div>
					<?php endforeach ?>
				</div>


				<!-- frAME -->
				</div>

			</div>

			<!-- <div class="card-deck">
			  <?php foreach ($all_hadiah as $hadiah): ?>
			  <div class="card">
			    <img class="card-img-top" src="<?= base_url() ?><?= $hadiah->foto ?>" alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title"><strong>No Hadiah : <?= $hadiah->kode_hadiah ?> </strong></h5>
			      <p class="card-text"><?= $hadiah->nama_hadiah?></p>
			    </div>
			    <div class="card-footer">
			   <small class="text-muted"><?= $hadiah->point ?></small>
			 </div>
			  </div>
				<?php endforeach ?>
			</div> -->
			<!-- load footer -->
			<?php $this->load->view('partials/footer.php') ?>
		</div>
	</div>
	<?php $this->load->view('partials/js.php') ?>
	<script src="<?= base_url('sb-admin/js/demo/datatables-demo.js') ?>"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
	<script src="<?= base_url('sb-admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
</body>
</html>
