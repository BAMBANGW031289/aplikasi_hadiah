<?php

class M_transaksi extends CI_Model {
	protected $_table = 'transaksi';

	public function lihat(){
		return $this->db->get($this->_table)->result();
	}

	public function jumlah(){
		$query = $this->db->get($this->_table);
		return $query->num_rows();
	}

	public function lihat_no_transaksi($no_transaksi){
		return $this->db->get_where($this->_table, ['no_transaksi' => $no_transaksi])->row();
	}


	function lihat_customer($cus){

        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where("nama_customer",$cus);
        return $this->db->get()->result_array();
}

	function lihat_hadiah($id){
	// 	$result = 0;
	//$querys = "SELECT SUM(point) as totalan FROM transaksi WHERE nama_customer = '$id'";
  //return $this->db->query($querys)->result();
	// return $s;
	// $this->db->get()->result_array();

	//$this->db->distinct();
        $this->db->select("SUM(point) as poin");
        $this->db->from($this->_table);
      //  $this->db->join("user","user.id_user=kartu.id_user");
        $this->db->where("nama_customer",$id);
        //return $this->db->get()->row(); //ubah ini
        return $this->db->get()->result_array();
}

	public function tambah($data){
		return $this->db->insert($this->_table, $data);
	}

	public function hapus($no_transaksi){
		return $this->db->delete($this->_table, ['no_transaksi' => $no_transaksi]);
	}
}
