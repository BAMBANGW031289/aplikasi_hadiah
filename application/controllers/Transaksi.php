<?php

use dompdf\dompdf;
//require APPPATH . '/libraries/dompdf/dompdf.php';
//use libraries\dompdf\dompdf;

class Transaksi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		if($this->session->login['role'] != 'kasir' && $this->session->login['role'] != 'admin' && $this->session->login['role'] != 'customer') redirect();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('M_barang', 'm_barang');
		$this->load->model('M_transaksi', 'm_transaksi');
		$this->load->model('M_customer', 'm_customer');
		$this->load->model('M_detail_transaksi', 'm_detail_transaksi');
		$this->data['aktif'] = 'transaksi';
	}

	public function index(){
		$this->data['title'] = 'Data Transaksi';
		$this->data['all_transaksi'] = $this->m_transaksi->lihat();

		$this->load->view('transaksi/lihat', $this->data);
	}

	public function customer(){
		$this->data['title'] = 'Data Transaksi';
		$this->data['all_transaksi2'] = $this->m_transaksi->lihat_customer($this->session->login['kode']);
		//echo json_encode($this->data);

		$this->load->view('transaksi/lihat_cus', $this->data);
	}

	public function tambah(){
		$this->data['title'] = 'Tambah Transaksi';
	  $this->data['all_customer'] = $this->m_customer->lihat();

		$this->load->view('transaksi/tambah', $this->data);
	}

	public function proses_tambah(){
		if ($this->session->login['role'] == 'kasir'){
			$this->session->set_flashdata('error', 'Tambah data hanya untuk admin!');
			redirect('transaksi');
		}

		$data = [
			'no_transaksi' => $this->input->post('no_transaksi'),
			'nama_admin' => $this->input->post('nama_admin'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'jam_transaksi' => $this->input->post('jam_transaksi'),
			'point' => $this->input->post('point'),
			'nama_customer' => $this->input->post('nama_customer'),
		];

		if($this->m_transaksi->tambah($data)){
			$this->session->set_flashdata('success', 'Data Transaksi <strong>Berhasil</strong> Ditambahkan!');
			redirect('transaksi');
		} else {
			$this->session->set_flashdata('error', 'Data Transaksi <strong>Gagal</strong> Ditambahkan!');
			redirect('transaksi');
		}
	}

	public function proses_tambah2(){
		$jumlah_barang_dibeli = count($this->input->post('nama_barang_hidden'));

		$data_transaksi = [
			'no_transaksi' => $this->input->post('no_transaksi'),
			'nama_kasir' => $this->input->post('nama_kasir'),
			'tgl_transaksi' => $this->input->post('tgl_transaksi'),
			'jam_transaksi' => $this->input->post('jam_transaksi'),
			'total' => $this->input->post('total_hidden'),
		];

		$data_detail_transaksi = [];

		for ($i=0; $i < $jumlah_barang_dibeli ; $i++) {
			array_push($data_detail_transaksi, ['nama_barang' => $this->input->post('nama_barang_hidden')[$i]]);
			$data_detail_transaksi[$i]['no_transaksi'] = $this->input->post('no_transaksi');
			$data_detail_transaksi[$i]['harga_barang'] = $this->input->post('harga_barang_hidden')[$i];
			$data_detail_transaksi[$i]['jumlah_barang'] = $this->input->post('jumlah_hidden')[$i];
			$data_detail_transaksi[$i]['satuan'] = $this->input->post('satuan_hidden')[$i];
			$data_detail_transaksi[$i]['sub_total'] = $this->input->post('sub_total_hidden')[$i];
		}

		if($this->m_transaksi->tambah($data_transaksi) && $this->m_detail_transaksi->tambah($data_detail_transaksi)){
			for ($i=0; $i < $jumlah_barang_dibeli ; $i++) {
				$this->m_barang->min_stok($data_detail_transaksi[$i]['jumlah_barang'], $data_detail_transaksi[$i]['nama_barang']) or die('gagal min stok');
			}
			$this->session->set_flashdata('success', 'Invoice <strong>Transaksi</strong> Berhasil Dibuat!');
			redirect('transaksi');
		} else {
			$this->session->set_flashdata('success', 'Invoice <strong>Transaksi</strong> Berhasil Dibuat!');
			redirect('transaksi');
		}
	}

	public function detail($no_transaksi){
		$this->data['title'] = 'Detail Transaksi';
		$this->data['transaksi'] = $this->m_transaksi->lihat_no_transaksi($no_transaksi);
		//$this->data['all_detail_transaksi'] = $this->m_detail_transaksi->lihat_no_transaksi($no_transaksi);
		$this->data['no'] = 1;

		$this->load->view('transaksi/detail', $this->data);
	}

	public function hapus($no_transaksi){
		if($this->m_transaksi->hapus($no_transaksi)){
			$this->session->set_flashdata('success', 'Invoice Transaksi <strong>Berhasil</strong> Dihapus!');
			redirect('transaksi');
		} else {
			$this->session->set_flashdata('error', 'Invoice Transaksi <strong>Gagal</strong> Dihapus!');
			redirect('transaksi');
		}
	}


	public function get_all_barang(){
		$data = $this->m_barang->lihat_nama_barang($_POST['nama_barang']);
		echo json_encode($data);
	}

	public function keranjang_barang(){
		$this->load->view('transaksi/keranjang');
	}

	public function export(){
	// define('DOMPDF_ENABLE_AUTOLOAD', false);
	// require_once("./libraries/dompdf/dompdf_config.inc.php");
		$dompdf = new dompdf();
		// $this->data['perusahaan'] = $this->m_usaha->lihat();
		$this->data['all_transaksi'] = $this->m_transaksi->lihat();
		$this->data['title'] = 'Laporan Data Transaksi';
		$this->data['no'] = 1;

		$dompdf->setPaper('A4', 'Landscape');
		$html = $this->load->view('transaksi/report', $this->data, true);
		$dompdf->load_html($html);
		$dompdf->render();
		$dompdf->stream('Laporan Data Transaksi Tanggal ' . date('d F Y'), array("Attachment" => false));
	}

	public function export_detail($no_transaksi){
	//	$this->load->library('PdfGenerator');
	// define('DOMPDF_ENABLE_AUTOLOAD', false);
	// require_once("Dompdf/dompdf_config.inc.php");
		//$dompdf = new dompdf();
		// $this->data['perusahaan'] = $this->m_usaha->lihat();
		$this->data['transaksi'] = $this->m_transaksi->lihat_no_transaksi($no_transaksi);
		$this->data['all_detail_transaksi'] = $this->m_detail_transaksi->lihat_no_transaksi($no_transaksi);
		$this->data['title'] = 'Laporan Detail Transaksi';
		$this->data['no'] = 1;

	//	$dompdf->setPaper('A4', 'Landscape');
		//$html = $this->load->view('transaksi/detail_report', $this->data, true);
				$this->load->view('transaksi/detail_report', $this->data, true);
	//	$dompdf->load_html($html);
	//	$dompdf->render();
	//	$dompdf->stream('Laporan Detail Transaksi Tanggal ' . date('d F Y'), array("Attachment" => false));

	//	$this->pdfgenerator->generate($html,'contoh');
	}
}
